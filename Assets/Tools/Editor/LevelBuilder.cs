﻿using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using System.Collections;
using System.IO;
using System.Collections.Generic;

namespace Voodoo_Test.Level
{
    public class LevelBuilder : MonoBehaviour
    {

        public static void BuildSingleLevel(string path)
        {
            AddCamera();
            CreateLevelManager(path);
            AddBallAndPaddle();
        }

        public static void CreateLevelManager(string path)
        {
            Object LMPrefab = Resources.Load("Prefabs/LevelManager");
            GameObject LMObject = PrefabUtility.InstantiatePrefab(LMPrefab, SceneManager.GetActiveScene()) as GameObject;

            LevelManager lm = LMObject.GetComponent<LevelManager>();
            lm.BuildLevelFromCSV(path);
        }

        public static void AddBallAndPaddle()
        {
            //create ball
            Object BallPrefab = Resources.Load("Prefabs/Ball");
            PrefabUtility.InstantiatePrefab(BallPrefab, SceneManager.GetActiveScene());

            //create paddle
            Object paddlePrefab = Resources.Load("Prefabs/Paddle");
            PrefabUtility.InstantiatePrefab(paddlePrefab, SceneManager.GetActiveScene());


            //create borders
            Object bordersPrefab = Resources.Load("Prefabs/Borders");
            PrefabUtility.InstantiatePrefab(bordersPrefab,SceneManager.GetActiveScene());

        }

        public static void AddCamera()
        {
            GameObject cameraObject = new GameObject("Main Camera");
            cameraObject.transform.position = new Vector3(0, 1, -10);
            cameraObject.AddComponent<Camera>();
            Camera cam = cameraObject.GetComponent<Camera>();
            cam.orthographic = true;
            cam.backgroundColor = Color.black;
            cam.clearFlags = CameraClearFlags.SolidColor;

        }
        public static List<string> LookForFiles()
        {
            string rootPath = Application.dataPath + "/LevelCSVs/";
            DirectoryInfo d = new DirectoryInfo(rootPath);
            List<string> paths = new List<string>();
            
            FileInfo[] f = d.GetFiles();

            //find all paths with csv files 
            foreach (FileInfo file in f)
                if (file.Extension == (".CSV"))
                {
                    paths.Add(Path.Combine(file.DirectoryName, file.FullName));
                }
            
            //print(paths[0]);
            return paths;
        }
    }
}
