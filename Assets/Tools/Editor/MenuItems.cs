﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Voodoo_Test.Level
{
    public class MenuItems : MonoBehaviour
    {
        [MenuItem ("Tools/Level/New Level")]
        private static void NewLevel()
        {
            EditorUtils.NewLevel("None");
        }

        [MenuItem("Tools/Level/Generate New Levels with files")]
        private static void GenerateLevels()
        {
             List <string> paths = LevelBuilder.LookForFiles();
            for (int i = 0; i< paths.Count;i++)
            {
                EditorUtils.NewLevel(paths[i]);

                EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene(),Application.dataPath+"/Scenes/GeneratedLevels/Scene"+i+".unity");

            }
            //EditorUtils.NewLevel();
        }

    }
}