﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Collections;

namespace Voodoo_Test.Level
{
    public class EditorUtils : MonoBehaviour
    {

        public static void NewScene()
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects);
        }

        public static void CleanScene()
        {
            GameObject[] allObjects = Object.FindObjectsOfType<GameObject>();
            foreach (GameObject g in allObjects)
                DestroyImmediate(g);
        }

        public static void NewLevel(string path)
        {
            NewScene();
            CleanScene();
            LevelBuilder.BuildSingleLevel(path);

        }

    }

}