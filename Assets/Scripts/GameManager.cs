﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour {
    public static int lives = 0;
    private int resets = 2;
	public static int totalBricks = 0;
    float InitTime = 0.0f;
    public static float timeTaken = 0.0f;
    public Text leftNum , livesLeft;
    // Use this for initialization
	void Awake () {
        InitTime = Time.time;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
	
	// Update is called once per frame
	void Update () {
        if (lives <= 0)
        {

            if (resets == 0)
                GameOver();
            else
            {
                Instantiate(Resources.Load("Prefabs/Ball"));
                resets--;
            }
        }
        if (totalBricks <= 0) LevelCleared();

        leftNum.text = totalBricks.ToString();
        livesLeft.text = resets.ToString();
        
	}

    void GameOver()
    {
        SceneManager.LoadScene("YouLose");
    }

    void LevelCleared()
    {
        print("Level Cleared");
        timeTaken = Time.time - InitTime;
        SceneManager.LoadScene("YouWin");
        
    }

    public static void resetAllStatics()
    {
        lives = 0;
        totalBricks = 0;

    }
}
