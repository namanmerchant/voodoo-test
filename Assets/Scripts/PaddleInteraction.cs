﻿using UnityEngine;
using System.Collections;

public class PaddleInteraction : MonoBehaviour {
    public float multiplyer = 5.0f;
    // Use this for initialization
    void Start () {
	
	}


    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            InteractWithBall(collision.gameObject.GetComponent<Ball>(), collision.contacts[0].point);

        }
    }

    protected virtual void InteractWithBall(Ball ball, Vector3 point)
    {
        float offset = (transform.position - point).x * multiplyer;
        ball.rig.velocity = new Vector2( - offset, ball.rig.velocity.y);
        ball.numberOfSolidHits = 0;

    }
}
