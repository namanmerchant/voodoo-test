﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GotoScene : MonoBehaviour {
    public string SceneName;
	// Use this for initialization
	public void gotoScene()
    {
        SceneManager.LoadScene(SceneName);
    }
}
