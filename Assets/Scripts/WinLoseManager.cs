﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WinLoseManager : MonoBehaviour {

    public Text TimeLeft;
	// Use this for initialization
	void Start () {
        GameManager.resetAllStatics();
        if (TimeLeft != null)
        {
            TimeLeft.text = GameManager.timeTaken.ToString();
        }
	}
	
    public void BackToLevelSelect()
    {
        SceneManager.LoadScene("LevelSelect");
    }
    
}
