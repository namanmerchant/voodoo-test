﻿using UnityEngine;
using System.Collections;

public class HitBottom : MonoBehaviour{

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            InteractWithBall(collision.gameObject.GetComponent<Ball>(), collision.contacts[0].normal);

        }
    }


    protected virtual void InteractWithBall(Ball ball, Vector2 normal)
    {
        //Rigidbody2D rig = ball.GetComponent<Rigidbody2D>();
        // rig.velocity = -normal * ball.speed;

        Destroy(ball.gameObject);
        GameManager.lives--;

    }
}
