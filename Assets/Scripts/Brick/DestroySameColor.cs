﻿using UnityEngine;
using System.Collections;

public class DestroySameColor : MonoBehaviour {
    LevelManager lm;
    Brick brick;
	// Use this for initialization
	void Start () {
        
        brick = GetComponent<Brick>();

        brick.VisualData.color1 = brick.VisualData.color1 * 2.0f;
        brick.VisualData.color2 = brick.VisualData.color2 * 2.0f;
        brick.VisualData.applyData(GetComponent<SpriteRenderer>().material);
    }


    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            //Get the array where bricks were stored initially and use the index color from the bricks
            foreach (Brick b in LevelManager.instance.AllColoredBricks[brick.index_color].list)
            {
                if(b != null)
                b.TakeDamage();
            }

        }
    }
    

}
