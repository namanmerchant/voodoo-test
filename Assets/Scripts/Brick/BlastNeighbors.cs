﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlastNeighbors : Brick {
    

    public override void TakeDamage(int damage = 1)
    {
        isHit = true;
        List<Brick> all = new List<Brick>();
        
        //make objects around burst
        for (int i = index_i - 1; i <= index_i + 1; i++)
        {
            for (int j = index_j - 1; j <= index_j + 1; j++)
            {
                if (i >= 0 && j >= 0 && i < 16 && j < 8)
                {
                    
                    Brick brk = LevelManager.instance.BricksGrid[i * 8 + j];
                    if (brk != null&&brk!=this&&!brk.isHit)
                    {
                        all.Add(brk);
                    }
                }
            }

        }
        StartCoroutine(blastEmAll(all));
        base.TakeDamage(damage);
    }

    IEnumerator blastEmAll(List<Brick> brk)
    {
        yield return new WaitForSeconds(0.1f);
        foreach (Brick b in brk)
        {
            if( !b.isHit)
            b.TakeDamage();
        }

    }
}
