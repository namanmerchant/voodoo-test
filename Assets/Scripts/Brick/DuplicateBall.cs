﻿using UnityEngine;
using System.Collections;

public class DuplicateBall : MonoBehaviour {
    Object ball;
    bool doonce = false;
	// Use this for initialization
	void Start () {
        ball = Resources.Load("Prefabs/Ball");
        GetComponent<SpriteRenderer>().sortingOrder = 15;
	}

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball" && !doonce)
        {
            doonce = true;
            GameObject[] gos = GameObject.FindGameObjectsWithTag("Ball");
            int len = gos.Length;
            //print(len);
            for(int i = 0;i<len;i++)
            {
                GameObject.Instantiate(ball, gos[i].transform.position+Vector3.up*0.1f, Quaternion.identity);
            }

        }
    }
}
