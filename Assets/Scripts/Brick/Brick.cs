﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class BrickVisuals
{

    public Color color1, color2, borderColor;
    public float lerpSpeed = 1, borderWidth = 0.045f,HSpeed,VSpeed = 1;
    public Texture2D overlayTexture;
    public bool HBorder = true, VBorder = true;
    //[Range(25,60)]
    public float HBorderFrequency = 40;


    //[Range(5, 15)]
    public float VBorderFrequency = 15;

    public void applyData(Material m)
    {
        


        m.SetColor("_Color1", color1);
        m.SetColor("_Color2", color2);
        m.SetFloat("_lerpSpeed", lerpSpeed);
        m.SetFloat("_BorderWidth", borderWidth);
        m.SetFloat("_hSpeed", HSpeed);
        m.SetFloat("_vSpeed", VSpeed);
        m.SetTexture("_OverlayTex", overlayTexture);
        m.SetInt("_HBorder", HBorder ? 1:0);
        m.SetInt("_VBorder", VBorder ? 1 : 0);
        m.SetFloat("_HBorderFrequency", HBorderFrequency);
        m.SetFloat("_VBorderFrequency", VBorderFrequency);
        m.SetColor("_BorderColor", borderColor);

    }
}


[RequireComponent (typeof(SpriteRenderer),typeof(BoxCollider2D))]
public class Brick : MonoBehaviour {
    [HideInInspector]
    public  bool isHit = false;

    [HideInInspector]
    public bool destructable = true;

    [HideInInspector]
    public int index_i, index_j, index_color;    // Easy to access grid around it if required

    protected SpriteRenderer spr;
    protected Rigidbody2D rb;
    Object particle;
    public BrickVisuals VisualData;
    public int Health = 1;


    protected virtual void Awake()
    {
        InitializeBrick();
        particle = Resources.Load("Prefabs/Particles/particles");
        //print(GetComponent<BoxCollider2D>().bounds.size);
    }

    // Use this for initialization
    protected virtual void Start () {
        spr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();

        VisualData.applyData(spr.material);
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}


    protected virtual void InitializeBrick()
    {
        if(destructable)
            GameManager.totalBricks++;
    }

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ball")
        {
            InteractWithBall(collision.gameObject.GetComponent<Ball>(), collision.contacts[0].normal);
            
        }
    }

    protected virtual void InteractWithBall(Ball ball, Vector2 normal)
    {
        //Rigidbody2D rig = ball.GetComponent<Rigidbody2D>();
        //rig.velocity = -normal * ball.speed ;
        TakeDamage();

        ball.numberOfSolidHits = (destructable)?0:ball.numberOfSolidHits+1;

        
    }

    public virtual void TakeDamage(int damage = 1)
    {
        isHit = true;
        Health -= damage;
        if (Health <= 0 && destructable)
            StartCoroutine(DestroyBrick(0.1f));
    }

    protected virtual IEnumerator DestroyBrick(float time)
    {
        GameObject part =  GameObject.Instantiate(particle, transform.position+transform.forward*-1.8f, Quaternion.identity) as GameObject;
        part.GetComponent<ParticleSystem>().startColor = VisualData.color1;
        spr.enabled = false;
        yield return new WaitForSeconds(time);
        GameManager.totalBricks--;
        Destroy(gameObject);
    }


    public virtual void MakeIndestructable()
    {
        destructable = false;

        VisualData.HBorder = true;
        VisualData.VBorder = true;
        VisualData.HBorderFrequency = 0;
        VisualData.VBorderFrequency = 0;
        VisualData.borderColor = Color.white;
        VisualData.color1 = Color.gray;
        VisualData.color2 = Color.gray*1.5f;

        GetComponent<SpriteRenderer>().sortingOrder = 15;


    }

}
