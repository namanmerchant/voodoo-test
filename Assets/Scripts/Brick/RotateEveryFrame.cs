﻿using UnityEngine;
using System.Collections;

public class RotateEveryFrame : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Rigidbody2D rig = GetComponent<Rigidbody2D>();
        rig.AddTorque(3.0f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
