﻿using UnityEngine;
using System.Collections;

public class CheckMovementBounds : MonoBehaviour {
    public float refreshRate = 2.0f;
    public float offsetMul = 1.001f;
    [HideInInspector]
    public float left, right;

    BoxCollider2D col;
    // Use this for initialization
    void Start () {

        InvokeRepeating("checkBorders", 0.0f, refreshRate);
        col = GetComponent<BoxCollider2D>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void checkBorders()
    {
        //Calculating from bounds of the box component offset 1.001f so that the raycast doesnt hit self.
        float boxHalfSize = (col.bounds.extents.x * offsetMul);

        //Raycasting to the right to find closest point to the right
        RaycastHit2D hit = Physics2D.Raycast(transform.position + Vector3.right * boxHalfSize, Vector2.right);
        
        if (hit.collider && hit.collider.gameObject.tag != "Ball")
        {
            right = hit.point.x - boxHalfSize;
        }

        //Doing the same on the left
        hit = Physics2D.Raycast(transform.position - Vector3.right * boxHalfSize, Vector2.left);

        if (hit.collider && hit.collider.gameObject.tag != "Ball")
        {
            left = hit.point.x + boxHalfSize;
        }

    }
}
