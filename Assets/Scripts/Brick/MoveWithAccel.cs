﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CheckMovementBounds))]
public class MoveWithAccel : MonoBehaviour {
    public float speed = 1;
    private CheckMovementBounds margins;
	// Use this for initialization
	void Start () {
        margins = GetComponent<CheckMovementBounds>();
	}
	
	// Update is called once per frame
	void Update () {

        if(Mathf.Abs(Input.acceleration.x)>0.01f)
            transform.position += ((Input.acceleration.x)-(Mathf.Sign(Input.acceleration.x)*0.01f)) * Vector3.right * speed * Time.deltaTime * 60;

#if UNITY_EDITOR
        transform.position += Input.GetAxis("Horizontal") * Vector3.right * speed * Time.deltaTime * 60;
#endif
        float newX = Mathf.Clamp(transform.position.x,margins.left,margins.right);
        transform.position = new Vector3(newX,transform.position.y,transform.position.z);
    }

    

}
