﻿using UnityEngine;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public struct DictionaryColors
{
    public char key;
    public Color col1, col2;
}

[System.Serializable]
public struct BricksWrapper
{
    public List<Brick> list;
}


public class LevelManager : MonoBehaviour {

    private int _Columns = 8;
    
    private int _Rows = 16;

    public float _ColWidth=1,_RowWidth=2;

    public Vector3 GridStartPosition;

    public List<GameObject> AllBricksPrefabs;

    public List<DictionaryColors> AllColors;

    public List<BricksWrapper> AllColoredBricks;

    public Brick[] BricksGrid;

    public static LevelManager instance;

    private void Awake()
    {
        instance = this;
    }

#if UNITY_EDITOR
    void initializeColorBrickArray()
    {

        AllColoredBricks = new List<BricksWrapper>();
        for (int i = 0; i < AllColors.Count; i++)
        {
            BricksWrapper bw = new BricksWrapper();
            bw.list = new List<Brick>();
            AllColoredBricks.Add(bw);
            
        }

        BricksGrid = new Brick[_Rows* _Columns];

    }
    public void BuildLevelFromCSV(string path)
    {
        initializeColorBrickArray();

        
        StringBuilder sb = new StringBuilder();
        //read from file
        string s = File.ReadAllText(path);
        sb.Append(s);

        print("Line 1 = ");
        string[,] str = new string[_Rows,_Columns];
        int j = 0, k = 0;
        for(int i = 0; i < sb.Length; i++)
        {
            if (sb[i] == ',')
            {
                j++;
                if(k < _Rows && j < _Columns)
                str[k, j] = "";
            }
            else if (sb[i] == '\n')
            {
                k++;
                j = 0;
                if (k < _Rows && j < _Columns)
                    str[k, j] = "";
            }
            else if (k < _Rows && j < _Columns)
                str[k, j] += sb[i];


        }

        string st = "";
        for (int i = 0; i < _Rows; i++)
        {
            for (int h = 0; h < _Columns; h++)
                st += str[ i,h]+" ";

            st += '\n';
        }
        print(st);
        InstantiateObjectsFromCSV(str);
        
    }

    void InstantiateObjectsFromCSV(string[,] str)
    {
        GameObject GridParent = new GameObject("Grid");
        GridParent.transform.position = GridStartPosition;
        for (int i = 0; i < _Rows; i++)
            for (int j = 0; j < _Columns; j++)
            {
                string st = str[i, j];
                if (st == "")     //999 is used as a blank value in the csv files so that the reading can be done properly
                    continue;

                if (st == "999")
                    continue;
                if (st == null)
                    continue;
                
                char[] s = st.ToCharArray();

                if (!char.IsDigit(s[0]))
                {
                   // UnityEditor.EditorUtility.DisplayDialog("Error", "The CSV File contains a syntax error at "+i+" , " +j+" string:"+st ,"Close");

                    //return;
                    continue;
                }
                int brickType = (int)char.GetNumericValue( s[0]);
                if (brickType >= AllBricksPrefabs.Count)
                    continue;

                //instantiate brick type from prefab of level manager
                GameObject brickObj = UnityEditor.PrefabUtility.InstantiatePrefab(AllBricksPrefabs[brickType], UnityEngine.SceneManagement.SceneManager.GetActiveScene()) as GameObject;
                brickObj.transform.position = new Vector3(j*_RowWidth, i * -_ColWidth, brickObj.transform.position.z);
                    
                //set parent
                brickObj.transform.SetParent(GridParent.transform, false);

                Brick brk = brickObj.GetComponent<Brick>();
                brk.index_i = i;
                brk.index_j = j;
                BricksGrid[(i *_Columns) +  (j)] = brk;
                if (brickType >= 5) //Dont change color or anything for duplicating ball and blaster
                    continue;
                if (s.Length >= 2)
                {
                    getColorFromChar(s[1], brk);
                    if (s.Length >= 3)
                    {
                        if(s[2] == 'N')
                        brk.MakeIndestructable();
                    }
                }
                    

                //set brick specific parameters
                


            }
    }
    void getColorFromChar(char c, Brick b){
        c = char.ToUpper(c);

        //set required colors from dictionary created in prefab
        int i = 0;
        foreach(DictionaryColors col in AllColors)
        {
            if(col.key == c)
            {
                b.VisualData.color1 = col.col1;
                b.VisualData.color2 = col.col2;
                AllColoredBricks[i].list.Add(b);
                b.index_color = i;
            }
            i++;
        }
    }
#endif

}
