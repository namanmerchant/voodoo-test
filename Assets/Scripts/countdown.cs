﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class countdown : MonoBehaviour {
    int val = 3;
    Text t;

    private void Start()
    {
        Time.timeScale = 0.0f;
        t = GetComponent<Text>();
    }
    // Use this for initialization
    public void changeText()
    {
        val--;
        t.text = val.ToString();
    }

    public void disable()
    {
        Time.timeScale = 1.0f;
        Destroy(gameObject);
    }
}
