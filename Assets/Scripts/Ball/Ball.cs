﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(SpriteRenderer),typeof(Rigidbody2D))]
public class Ball : MonoBehaviour {

    SpriteRenderer spr;
    [HideInInspector]
    public Rigidbody2D rig;
    TrailRenderer trail;
    public float speed = 2;

    public int numberOfSolidHits = 0;
    private int threshold = 30;
	// Use this for initialization
	void Awake () {
        spr = GetComponent<SpriteRenderer>();
        rig = GetComponent<Rigidbody2D>();
        trail = GetComponent<TrailRenderer>();
        rig.velocity = Vector2.up*speed;

        GameManager.lives++;
        InitTrail();

	}
	
    protected virtual void InitTrail()
    {
        trail.sortingLayerName = "Background";
        trail.sortingOrder = 2;
    }

    private void Update()
    {
        if (numberOfSolidHits > threshold)
        {
            resetPosition();
        }
    }
    // Update is called once per frame
    void FixedUpdate () {

        //if (rig.velocity.SqrMagnitude() < (speed*speed))
         rig.velocity = rig.velocity.normalized * speed;
        
	}

    public void resetPosition()
    {
        transform.position = new Vector3(0.03f,-3.27f,0);
        rig.velocity = Vector2.up * speed;
        numberOfSolidHits = 0;
    }
}
