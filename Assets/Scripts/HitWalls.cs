﻿using UnityEngine;
using System.Collections;

public class HitWalls : MonoBehaviour {

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {

            collision.gameObject.GetComponent<Ball>().numberOfSolidHits++;

        }
    }
    
}
