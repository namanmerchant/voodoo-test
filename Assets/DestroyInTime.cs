﻿using UnityEngine;
using System.Collections;

public class DestroyInTime : MonoBehaviour {
    public float time = 3;
	// Use this for initialization
	void Start () {
        Destroy(gameObject, time);
	}
	
}
