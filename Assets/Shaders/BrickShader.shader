﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/BrickShader"
{
	Properties
	{
		_Color1("Main Color", COLOR) = (1,1,1,1)
		_Color2("Lerp Color", COLOR) = (1,1,1,1)
		_lerpSpeed("Lerp Speed", FLOAT) = 1
		_hSpeed("Horizontal Speed", Range(-5,5)) = 0
		_vSpeed("Vertical Speed", Range(-5,5)) = 0
		_HBorder("Enable Horizontal Border", Int) = 1
		_HBorderFrequency("Horizontal Border frequency", FLOAT) = 40
		_VBorder("Enable Vertical Border", Int) = 1
		_VBorderFrequency("Vertical Border frequency", FLOAT) = 15
		_Border("Border Offset",RANGE(0,1)) = 0
		_BorderColor("Border Color", COLOR) = (1,1,1,1)
		_MainTex("Texture", 2D) = "white" {}
		_OverlayTex("Texture0",2D) = "white" {}
		_TexScale("Overlay Texture Scale", FLOAT) = 1



	}
	SubShader
	{
		Tags { "Queue" = "Transparent" "RenderType"="Transparent" }
		LOD 100

		Pass
		{

			ColorMask RGB
			Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			//#pragma surface frag Standard fullforwardshadows alpha
			// make fog work
			//#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float3 worldPos : TEXCOORD1;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			sampler2D _OverlayTex;
			float4 _OverlayTex_ST;
			float4 _Color1, _Color2, _BorderColor;
			float _Border, _hSpeed, _vSpeed, _HBorder, _VBorder, _HBorderFrequency, _VBorderFrequency, _lerpSpeed, _TexScale;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);

				o.worldPos = mul(_Object2World, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				
				float t = sin(_Time*10.0f*_lerpSpeed);
				t += 1.0f;
				t = t / 2;
				col = lerp(_Color1,_Color2,t);


				fixed2 p = (i.worldPos.xy);
				p.x = p.x /(7*_TexScale);
				p.y = p.y / (7 *_TexScale);
				p.x += _Time*_hSpeed;
				p.y += _Time*_vSpeed;

				//Multiplying the color with the overlay texture in world space
				fixed4 col2 = tex2D(_OverlayTex, p);
				col *= (col2*2.010f);



				//Vertical Borders
				float4 newColor = _BorderColor;
				float dir = lerp(-1, 1, i.uv.x < 0.5f);
				//newColor.rgb *= (t+0.5f)/2;
				float newBorderH = _Border + (sin(((i.uv.y + (_Time*2*dir)) * _VBorderFrequency)) / 50.0f) ;
				col = lerp(col,newColor,(i.uv.x<newBorderH||i.uv.x>1- newBorderH)&& _VBorder);

				//Horizontal Borders

				dir = lerp(1,-1,i.uv.y < 0.5f);
				float newBorderV = _Border + (sin((i.uv.x + (_Time * dir)) * _HBorderFrequency) / 30.0f ) ;
				col = lerp(col, newColor, (i.uv.y<newBorderV || i.uv.y>1 - newBorderV) && _HBorder );
				

				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
